import { _decorator, Component, Node } from 'cc';
import { Spawner } from './spawner';
const { ccclass, property } = _decorator;

@ccclass('Controller')
export class Controller extends Component {
    @property(Spawner)
    public readonly spawner?: Spawner;

    start() {
        this.node.on(Node.EventType.TOUCH_START, this.onTouchEnd, this);
    }

    onTouchEnd() {
        this.spawner?.tap();
    }
}