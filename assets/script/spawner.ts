import { _decorator, Component, Node, instantiate, math, tween, lerp, MeshRenderer, Color, Tween, director, Vec3, Material, AudioSource, Label } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Spawner')
export class Spawner extends Component {
    @property(Node)
    public readonly basePlatform?: Node;

    @property(Node)
    public readonly platformGroup?: Node;

    @property(Node)
    public readonly dropChunkGroup?: Node;

    @property(Number)
    public readonly colorSteps = 10;

    @property(AudioSource)
    public readonly chopSfx?: AudioSource;

    @property(AudioSource)
    public readonly gameOverSfx?: AudioSource;

    @property(Label)
    public readonly scoreText?: Label;

    @property(Node)
    public readonly gameOverNode?: Node;

    private colorStepCtr = 0;

    private startColor = new Color(0, 0, 0);

    private endColor = new Color(
        Math.random() * 255, 
        Math.random() * 255, 
        Math.random() * 255
    );

    private randomSpawn = [
        [
            [
                {
                    x: -4,
                    z: 0,
                },
                {
                    x: 4,
                    z: 0,
                },
                {
                    x: -4,
                    z: 0,
                },
            ],
            [
                {
                    x: 4,
                    z: 0,
                },
                {
                    x: -4,
                    z: 0,
                },
                {
                    x: 4,
                    z: 0,
                },
            ]
        ],
        [
            [
                {
                    x: 0,
                    z: -4,
                },
                {
                    x: 0,
                    z: 4,
                },
                {
                    x: 0,
                    z: -4,
                },
            ],
            [
                {
                    x: 0,
                    z: 4,
                },
                {
                    x: 0,
                    z: -4,
                },
                {
                    x: 0,
                    z: 4,
                }
            ]
        ]
    ];

    private randomSpawnCtr = 0;

    private platformY = 0;

    private platformTween?: Tween<Node>;

    private prevPlatform?: Node;

    private currentPlatform?: Node;

    private speed = 1.0;

    private maxSpeed = 10.0;

    private isOver = false;

    private score = 0;
    
    start() {
        this.begin();
    }

    private begin() {
        const basePlatformMat = this.basePlatform?.getComponent(MeshRenderer)?.material;
        basePlatformMat?.setProperty('albedo', this.getRandomizedColor());

        this.prevPlatform = this.basePlatform;

        this.spawnPlatform();
    }

    private spawnPlatform() {
        const { basePlatform, platformGroup } = this;

        if (!basePlatform || !platformGroup) return;

        let { y: height } = basePlatform.scale;

        this.platformY += height;
        const randomSpawn = this.getRandomSpawn();

        this.currentPlatform = instantiate(basePlatform);
        const mat = this.currentPlatform.getComponent(MeshRenderer)?.material;
        this.currentPlatform.setParent(platformGroup);
        this.currentPlatform.setScale(
            this.prevPlatform?.scale.x || 0,
            this.prevPlatform?.scale.y || 0,
            this.prevPlatform?.scale.z || 0,
        );
        mat?.setProperty('albedo', this.getRandomizedColor());
        
        this.platformTween = tween(this.currentPlatform);
        for (let i = 1; i < randomSpawn.length; i++) {
            let { x: prevX, z: prevZ } = randomSpawn[i - 1];
            prevX += this.prevPlatform?.position.x || 0;
            prevZ += this.prevPlatform?.position.z || 0;

            let { x, z } = randomSpawn[i];
            x += this.prevPlatform?.position.x || 0;
            z += this.prevPlatform?.position.z || 0;
            this.platformTween.to(
                3 / this.speed,
                {},
                {
                    onUpdate: (_, ratio = 1) => {
                        this.currentPlatform?.setPosition(
                            lerp(prevX, x, ratio),
                            this.platformY,
                            lerp(prevZ, z, ratio)
                        );
                    }
                }
            );
        }
        this.platformTween.union().repeatForever().start();
    }

    private handlePlatformStop() {
        this.platformTween?.stop();
        this.platformTween = undefined;

        this.processPlatformIntersect();

        this.prevPlatform = this.currentPlatform;
    }

    private isGameOver() {
        if (!this.currentPlatform) return false;

        return this.currentPlatform.scale.x < 0 || this.currentPlatform.scale.z < 0;
    }

    private processPlatformIntersect() {
        const { prevPlatform, currentPlatform } = this;

        if (!prevPlatform || !currentPlatform) return;

        const { x: prevX, z: prevZ } = prevPlatform.position;
        const { x: prevWidth, z: prevBreadth } = prevPlatform.scale;

        const { x, y, z } = currentPlatform.position;
        const { x: width, y: height, z: breadth } = currentPlatform.scale;

        const isHorizontal = prevX !== x;

        if (isHorizontal) {
            const prevBounds = {
                left: prevX - prevWidth * 0.5,
                right: prevX + prevWidth * 0.5,
            };

            const bounds = {
                left: x - width * 0.5,
                right: x + width * 0.5,
            };

            if (prevX > x) {
                const newWidth = bounds.right - prevBounds.left;
                const newX = prevBounds.left + newWidth * 0.5;

                currentPlatform.setPosition(newX, y, z);
                currentPlatform.setScale(newWidth, height, breadth);

                if (prevBounds.left < bounds.right) {
                    this.spawnDropChunk(
                        currentPlatform,
                        (bounds.left + prevBounds.left) * 0.5,
                        z,
                        prevBounds.left - bounds.left,
                        height,
                        breadth,
                        bounds.left * 5,
                        z,
                        isHorizontal,
                    );
                }
            } else {
                const newWidth = prevBounds.right - bounds.left;
                const newX = bounds.left + newWidth * 0.5;

                currentPlatform.setPosition(newX, y, z);
                currentPlatform.setScale(newWidth, height, breadth);

                if (prevBounds.right > bounds.left) {
                    this.spawnDropChunk(
                        currentPlatform,
                        (prevBounds.right + bounds.right) * 0.5,
                        z,
                        bounds.right - prevBounds.right,
                        height,
                        breadth,
                        bounds.right * 5,
                        z,
                        isHorizontal,
                    );
                }
            }
        } else {
            const prevBounds = {
                top: prevZ - prevBreadth * 0.5,
                bottom: prevZ + prevBreadth * 0.5,
            };

            const bounds = {
                top: z - breadth * 0.5,
                bottom: z + breadth * 0.5,
            };

            if (prevZ > z) {
                const newBreadth = bounds.bottom - prevBounds.top;
                const newZ = prevBounds.top + newBreadth * 0.5;

                currentPlatform.setPosition(x, y, newZ);
                currentPlatform.setScale(width, height, newBreadth);

                if (prevBounds.top < bounds.bottom) {
                    this.spawnDropChunk(
                        currentPlatform,
                        x,
                        (bounds.top + prevBounds.top) * 0.5,
                        width,
                        height,
                        prevBounds.top - bounds.top,
                        x,
                        bounds.top * 5,
                        isHorizontal,
                    );
                }
            } else {
                const newBreadth = prevBounds.bottom - bounds.top;
                const newZ = bounds.top + newBreadth * 0.5;

                currentPlatform.setPosition(x, y, newZ);
                currentPlatform.setScale(width, height, newBreadth);

                if (prevBounds.bottom > bounds.top) {
                    this.spawnDropChunk(
                        currentPlatform,
                        x,
                        (prevBounds.bottom + bounds.bottom) * 0.5,
                        width,
                        height,
                        bounds.bottom - prevBounds.bottom,
                        x,
                        bounds.bottom * 5,
                        isHorizontal,
                    );
                }
            }
        }
    }

    private spawnDropChunk(
        source: Node,
        x: number,
        z: number,
        width: number,
        height: number,
        breadth: number,
        targetX: number,
        targetZ: number,
        isHorizontal: boolean,
    ) {
        const { dropChunkGroup } = this;

        if (!dropChunkGroup) return;

        const dropChunk = instantiate(source);
        const mat = dropChunk.getComponent(MeshRenderer)?.material;
        mat?.setProperty('albedo', source.getComponent(MeshRenderer)?.material?.getProperty('albedo') || 0);
        dropChunk.setPosition(x, -30, z);
        dropChunk.setScale(width, height, breadth);

        dropChunk.setParent(dropChunkGroup);

        const rotateTo = math.randomRange(180, 720);

        tween(dropChunk).to(
            1.5,
            {},
            {
                onUpdate: (_, ratio = 1) => {
                    dropChunk.setPosition(
                        lerp(x, targetX, ratio),
                        lerp(0.5, -30, ratio),
                        lerp(z, targetZ, ratio),
                    );

                    if (isHorizontal) {
                        dropChunk.setRotationFromEuler(
                            0,
                            0,
                            lerp(0, rotateTo, ratio),
                        );
                    } else {
                        dropChunk.setRotationFromEuler(
                            lerp(0, rotateTo, ratio),
                            0,
                            0,
                        );
                    }
                },
                onComplete: () => {
                    dropChunk.destroy();
                }
            }
        ).start();

        this.chopSfx?.play();
    }

    public stopPlatformTween() {
        if (this.platformTween) {
            this.handlePlatformStop();
            if (this.isGameOver()) {
                this.handleGameOver();
            } else {
                this.addScore();
                this.progressSpeed();
                this.tweenPlatformY();
            } 
        }
    }

    private addScore() {
        this.score += 1;
        if (this.scoreText) {
            this.scoreText.string = `${this.score}`;
        }
    }

    private handleGameOver() {
        this.currentPlatform?.destroy();
        this.gameOverSfx?.play();

        if (this.gameOverNode) {
            this.gameOverNode.active = true;
        }
        this.scheduleOnce(() => {
            this.isOver = true;
        }, 1);
    }

    private progressSpeed() {
        this.speed = Math.min(this.speed * 1.01, this.maxSpeed);
    }

    private tweenPlatformY() {
        const { platformGroup } = this;

        if (!platformGroup) return;

        const { y: platformGroupY } = platformGroup.position;
        tween(platformGroup).to(
            0.25,
            {},
            {
                onUpdate: (_, ratio = 1) => {
                    platformGroup.setPosition(
                        0, 
                        lerp(platformGroupY, -this.platformY, ratio), 
                        0
                    );
                },
                onComplete: () => {
                    this.spawnPlatform();
                }
            }
        ).start();
    }

    private getRandomizedColor() {
        if (this.colorStepCtr === 0) {
            this.startColor.set(
                this.endColor.r,
                this.endColor.g,
                this.endColor.b
            );

            this.endColor.set(
                Math.random() * 255,
                Math.random() * 255,
                Math.random() * 255
            );
        }
        const ratio = this.colorStepCtr / (this.colorSteps - 1);
        const color = new Color(
            lerp(this.startColor.r, this.endColor.r, ratio),
            lerp(this.startColor.g, this.endColor.g, ratio),
            lerp(this.startColor.b, this.endColor.b, ratio),
        )
        this.colorStepCtr = (this.colorStepCtr + 1) % this.colorSteps;
        return color;
    }

    private getRandomSpawn() {
        const randomSpawnArr = this.randomSpawn[this.randomSpawnCtr];
        const randomSpawn = randomSpawnArr[Math.floor(Math.random() * randomSpawnArr.length)];

        this.randomSpawnCtr = (this.randomSpawnCtr + 1) % this.randomSpawn.length;

        return randomSpawn;
    }

    public tap() {
        if (this.isOver) { 
            director.loadScene('game');
        } else {
            this.stopPlatformTween();
        }
    }
}