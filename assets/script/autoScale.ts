import { _decorator, Component, Node, view } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('AutoScale')
export class AutoScale extends Component {
    update() {
        const scaleWidth = view.getVisibleSize().width / view.getDesignResolutionSize().width;
        const scale = Math.min(scaleWidth, 1);

        this.node.setScale(scale, scale);
    }
}