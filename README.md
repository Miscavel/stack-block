# Stack Block

Game can be played at: https://miscavel.gitlab.io/stack-block/

## Running on local

To run the project on your local device, you can host the `build` folder using any http-server of your choice.

For example, using the [serve](https://www.npmjs.com/package/serve) npm package:

`npm install -g serve`

`serve build`

## Opening the project

The project is built using Cocos Creator v3.2.0.

- To open the project, first download the `Cocos Creator Dashboard` from https://www.cocos.com/en/creator.

- Using the dashboard, navigate to the editor menu and download the 3.2.0 version.

<img src="/screenshot/download_editor.png" width="600">

- Press the `add` button under the `project` tab to add an existing project.

<img src="/screenshot/add_project.png" width="600">

- Select the directory where the project is cloned to.

<img src="/screenshot/select_project.png" width="600">

- Once the project is added, click the project to open it.

<img src="/screenshot/click_project.png" width="600">

- Once the editor is fully loaded and you get the following view, the project is successfully opened.

<img src="/screenshot/editor.png" width="600">

- Click the `play` button at the top of the editor next to the `Current scene` text to open the development version of the project.
